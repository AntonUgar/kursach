﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLib
{
    public class Properties
    {
        public static double D2_airCountOnChill(double B6_rashodDryClearedGasForNornalSituation, double B7_temperatureComeOutGasForOchistka, double B16_gasQUnderHisAllowedTemperature, double B9_allowedTemperatureWorkClearedGas, double B15_gasQUnderHisTemperature, double B17_QAtmosphereAirUnderAllowedGasTemperature, double B8_airTemperature)
        {
            return B6_rashodDryClearedGasForNornalSituation * ((B15_gasQUnderHisTemperature * B7_temperatureComeOutGasForOchistka - B16_gasQUnderHisAllowedTemperature * B9_allowedTemperatureWorkClearedGas) / (B15_gasQUnderHisTemperature * B7_temperatureComeOutGasForOchistka - B17_QAtmosphereAirUnderAllowedGasTemperature * B8_airTemperature));
        }

        public static double D3_TotalAmountGas_Vog(double B6_rashodDryClearedGasForNornalSituation, double B7_temperatureComeOutGasForOchistka, double B16_gasQUnderHisAllowedTemperature, double B9_allowedTemperatureWorkClearedGas, double B15_gasQUnderHisTemperature, double B17_QAtmosphereAirUnderAllowedGasTemperature, double B8_airTemperature)
        {
            double d2 = D2_airCountOnChill(B6_rashodDryClearedGasForNornalSituation, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature);
            return B6_rashodDryClearedGasForNornalSituation + d2;
        }


        public static double D4_rashodUnderWorkingCase(double B20_BarometricPressureGround_Pbar, double B21_ExcessGasPressure_Rg, double B6_rashodDryClearedGasForNornalSituation, double B7_temperatureComeOutGasForOchistka, double B16_gasQUnderHisAllowedTemperature, double B9_allowedTemperatureWorkClearedGas, double B15_gasQUnderHisTemperature, double B17_QAtmosphereAirUnderAllowedGasTemperature, double B8_airTemperature)
        {
            double d3 = D3_TotalAmountGas_Vog(B6_rashodDryClearedGasForNornalSituation, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature);
            double a = 273;
            return (d3 * (101.3 * (a + B9_allowedTemperatureWorkClearedGas)) / (a * (B20_BarometricPressureGround_Pbar - B21_ExcessGasPressure_Rg)));
        } 

        public static double D5_Vg(double B6_rashodDryClearedGasForNornalSituation, double B18_waterSteamWhichContainsInOutComingGas, double B19_waterSteamWhichContainsInAtmosphereAir, double B20_BarometricPressureGround_Pbar, double B7_temperatureComeOutGasForOchistka, double B16_gasQUnderHisAllowedTemperature, double B9_allowedTemperatureWorkClearedGas, double B15_gasQUnderHisTemperature, double B17_QAtmosphereAirUnderAllowedGasTemperature, double B8_airTemperature, double B21_ExcessGasPressure_Rg)
        {
            double d2 = D2_airCountOnChill(B6_rashodDryClearedGasForNornalSituation, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature);
            double d3 = D3_TotalAmountGas_Vog(B6_rashodDryClearedGasForNornalSituation, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature);
            double d4 = D4_rashodUnderWorkingCase(B20_BarometricPressureGround_Pbar, B21_ExcessGasPressure_Rg, B6_rashodDryClearedGasForNornalSituation, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature);

            return ((d4 / d3) * (B6_rashodDryClearedGasForNornalSituation * (1 + B18_waterSteamWhichContainsInOutComingGas / 0.804) + d2 * (1 + B19_waterSteamWhichContainsInAtmosphereAir / 0.804)));


        }

        public static double D6_p_o_g_vl(double B10_gasDensityNormalSituation, double B18_waterSteamWhichContainsInOutComingGas)
        {
            return 0.804 * (B10_gasDensityNormalSituation + B18_waterSteamWhichContainsInOutComingGas) / (0.804 + B18_waterSteamWhichContainsInOutComingGas);

        }


        public static double D7_PlotnostWetGasUnderNormal(double B11_airDensityNormalSituation, double B19_waterSteamWhichContainsInAtmosphereAir)
        {
            return (0.804 * (B11_airDensityNormalSituation + B19_waterSteamWhichContainsInAtmosphereAir) / (0.804 + B19_waterSteamWhichContainsInAtmosphereAir));

        }

        public static double D8_p_o_vl(double B10_gasDensityNormalSituation, double B18_waterSteamWhichContainsInOutComingGas, double B6_rashodDryClearedGasForNornalSituation, double B7_temperatureComeOutGasForOchistka, double B11_airDensityNormalSituation, double B9_allowedTemperatureWorkClearedGas, double B15_gasQUnderHisTemperature, double B17_QAtmosphereAirUnderAllowedGasTemperature, double B8_airTemperature, double B16_gasQUnderHisAllowedTemperature, double B19_waterSteamWhichContainsInAtmosphereAir)
        {
            double d2 = D2_airCountOnChill(B6_rashodDryClearedGasForNornalSituation, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature);
            double d3 = D3_TotalAmountGas_Vog(B6_rashodDryClearedGasForNornalSituation, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature);
            double d6 = D6_p_o_g_vl(B10_gasDensityNormalSituation, B18_waterSteamWhichContainsInOutComingGas);
            double d7 = D7_PlotnostWetGasUnderNormal(B11_airDensityNormalSituation, B19_waterSteamWhichContainsInAtmosphereAir);

            return d6 * (B6_rashodDryClearedGasForNornalSituation / d3) + d7 * (d2 / d3);
        }


        public static double D9_p_o_g_vl(double B20_BarometricPressureGround_Pbar, double B18_waterSteamWhichContainsInOutComingGas, double B9_allowedTemperatureWorkClearedGas, double B21_ExcessGasPressure_Rg, double B10_gasDensityNormalSituation, double B6_rashodDryClearedGasForNornalSituation, double B7_temperatureComeOutGasForOchistka, double B11_airDensityNormalSituation, double B19_waterSteamWhichContainsInAtmosphereAir, double B16_gasQUnderHisAllowedTemperatur, double B15_gasQUnderHisTemperature, double B17_QAtmosphereAirUnderAllowedGasTemperature, double B8_airTemperature, double B16_gasQUnderHisAllowedTemperature)
        {
            double d8 = D8_p_o_vl(B10_gasDensityNormalSituation, B18_waterSteamWhichContainsInOutComingGas, B6_rashodDryClearedGasForNornalSituation, B7_temperatureComeOutGasForOchistka, B11_airDensityNormalSituation, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature, B16_gasQUnderHisAllowedTemperature, B19_waterSteamWhichContainsInAtmosphereAir);

            return d8 * 273 * (B20_BarometricPressureGround_Pbar - B21_ExcessGasPressure_Rg) / ((B9_allowedTemperatureWorkClearedGas + 273) * 101.3);
        }

        public static double D10_u_gas(double B12_gasViskosityNormalSituation, double B3_sterlingConstGas, double B9_allowedTemperatureWorkClearedGas)
        {
            return B12_gasViskosityNormalSituation * ((273 + B3_sterlingConstGas) / (B9_allowedTemperatureWorkClearedGas + 273 + B3_sterlingConstGas)) * Math.Pow(((B9_allowedTemperatureWorkClearedGas + 273) / 273), 1.5);
        }

        public static double D11_u_vosduh(double B13_airViskosityNormalSituation, double B4_sterlingConstAir, double B9_allowedTemperatureWorkClearedGas)
        {
            return B13_airViskosityNormalSituation * ((273 + B4_sterlingConstAir) / (B9_allowedTemperatureWorkClearedGas + 273 + B4_sterlingConstAir)) * Math.Pow(((B9_allowedTemperatureWorkClearedGas + 273) / 273), 1.5);
        }
        public static double D12_u_voda(double B14_waterViskosityNormalSituation, double B5_sterlingConstWater, double B9_allowedTemperatureWorkClearedGas)
        {
            return B14_waterViskosityNormalSituation * ((273 + B5_sterlingConstWater) / (B9_allowedTemperatureWorkClearedGas + 273 + B5_sterlingConstWater)) * Math.Pow(((B9_allowedTemperatureWorkClearedGas + 273) / 273), 1.5);
        }

        public static double D13_u_smeshan_gas(double B6_rashodDryClearedGasForNornalSituation, double B18_waterSteamWhichContainsInOutComingGas, double B19_waterSteamWhichContainsInAtmosphereAir, double B20_BarometricPressureGround_Pbar, double B7_temperatureComeOutGasForOchistka, double B16_gasQUnderHisAllowedTemperature, double B9_allowedTemperatureWorkClearedGas, double B15_gasQUnderHisTemperature, double B17_QAtmosphereAirUnderAllowedGasTemperature, double B8_airTemperature, double B21_ExcessGasPressure_Rg, double B12_gasViskosityNormalSituation, double B3_sterlingConstGas, double B13_airViskosityNormalSituation, double B4_sterlingConstAir, double B14_waterViskosityNormalSituation, double B5_sterlingConstWater)
        {
            double d2 = D2_airCountOnChill(B6_rashodDryClearedGasForNornalSituation, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature);
            double d3 = D3_TotalAmountGas_Vog(B6_rashodDryClearedGasForNornalSituation, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature);
            double d4 = D4_rashodUnderWorkingCase(B20_BarometricPressureGround_Pbar, B21_ExcessGasPressure_Rg, B6_rashodDryClearedGasForNornalSituation, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature);
            double d5 = D5_Vg(B6_rashodDryClearedGasForNornalSituation, B18_waterSteamWhichContainsInOutComingGas, B19_waterSteamWhichContainsInAtmosphereAir, B20_BarometricPressureGround_Pbar, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature, B21_ExcessGasPressure_Rg);
            double d10 = D10_u_gas(B12_gasViskosityNormalSituation, B3_sterlingConstGas, B9_allowedTemperatureWorkClearedGas);
            double d11 = D11_u_vosduh(B13_airViskosityNormalSituation, B4_sterlingConstAir, B9_allowedTemperatureWorkClearedGas);
            double d12 = D12_u_voda(B14_waterViskosityNormalSituation, B5_sterlingConstWater, B9_allowedTemperatureWorkClearedGas);

            return d10 * B6_rashodDryClearedGasForNornalSituation / d3 + d11 * d2 / d3 + d12 * d4 / d5;
        }

        public static double D14_Z(double B23_DustConcentrationInWasteGas_Zo, double B6_rashodDryClearedGasForNornalSituation, double B18_waterSteamWhichContainsInOutComingGas, double B19_waterSteamWhichContainsInAtmosphereAir, double B20_BarometricPressureGround_Pbar, double B7_temperatureComeOutGasForOchistka, double B16_gasQUnderHisAllowedTemperature, double B9_allowedTemperatureWorkClearedGas, double B15_gasQUnderHisTemperature, double B17_QAtmosphereAirUnderAllowedGasTemperature, double B8_airTemperature, double B21_ExcessGasPressure_Rg)
        {
            double d5 = D5_Vg(B6_rashodDryClearedGasForNornalSituation, B18_waterSteamWhichContainsInOutComingGas, B19_waterSteamWhichContainsInAtmosphereAir, B20_BarometricPressureGround_Pbar, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature, B21_ExcessGasPressure_Rg);
            return B23_DustConcentrationInWasteGas_Zo * B6_rashodDryClearedGasForNornalSituation / d5;
        }

        public static double D15_qf(double B22_AverageDiameterDustParticles_dm, double B33_standardGasFilterLoad, double D16_K1, double B23_DustConcentrationInWasteGas_Zo, double B6_rashodDryClearedGasForNornalSituation, double B18_waterSteamWhichContainsInOutComingGas, double B19_waterSteamWhichContainsInAtmosphereAir, double B20_BarometricPressureGround_Pbar, double B7_temperatureComeOutGasForOchistka, double B16_gasQUnderHisAllowedTemperature, double B9_allowedTemperatureWorkClearedGas, double B15_gasQUnderHisTemperature, double B17_QAtmosphereAirUnderAllowedGasTemperature, double B8_airTemperature, double B21_ExcessGasPressure_Rg)
        {
            double d17 = D17_K2(B23_DustConcentrationInWasteGas_Zo, B6_rashodDryClearedGasForNornalSituation, B18_waterSteamWhichContainsInOutComingGas, B19_waterSteamWhichContainsInAtmosphereAir, B20_BarometricPressureGround_Pbar, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature, B21_ExcessGasPressure_Rg);
            double d18 = D18_K3(B22_AverageDiameterDustParticles_dm);
            double d19 = D19_K4(B9_allowedTemperatureWorkClearedGas);
            double d20 = D20_K5(B23_DustConcentrationInWasteGas_Zo);
            return D16_K1 * d17 * d18 * d19 * d20 * B33_standardGasFilterLoad;
        }
        public static double D17_K2(double B23_DustConcentrationInWasteGas_Zo, double B6_rashodDryClearedGasForNornalSituation, double B18_waterSteamWhichContainsInOutComingGas, double B19_waterSteamWhichContainsInAtmosphereAir, double B20_BarometricPressureGround_Pbar, double B7_temperatureComeOutGasForOchistka, double B16_gasQUnderHisAllowedTemperature, double B9_allowedTemperatureWorkClearedGas, double B15_gasQUnderHisTemperature, double B17_QAtmosphereAirUnderAllowedGasTemperature, double B8_airTemperature, double B21_ExcessGasPressure_Rg)
        {
            double d14 = D14_Z(B23_DustConcentrationInWasteGas_Zo, B6_rashodDryClearedGasForNornalSituation, B18_waterSteamWhichContainsInOutComingGas, B19_waterSteamWhichContainsInAtmosphereAir, B20_BarometricPressureGround_Pbar, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature, B21_ExcessGasPressure_Rg);
            return 0.983 * Math.Exp(-0.00169 * d14); 
        }


        public static double D18_K3(double B22_AverageDiameterDustParticles_dm)
        {
            if (B22_AverageDiameterDustParticles_dm > 100)
            {
                return 1.3;
            }
            else if (B22_AverageDiameterDustParticles_dm > 50 && B22_AverageDiameterDustParticles_dm <= 100)
            {
                return 1.1;
            }
            else if (B22_AverageDiameterDustParticles_dm > 10 && B22_AverageDiameterDustParticles_dm <= 50)
            {
                return 1;
            }

            else if (B22_AverageDiameterDustParticles_dm >= 3 && B22_AverageDiameterDustParticles_dm <= 10)
            {
                return 0.9;
            }
            else if (B22_AverageDiameterDustParticles_dm < 3)
            {
                return 0.75;
            }
            return 0;

        }

        public static double D19_K4(double B9_allowedTemperatureWorkClearedGas)
        {
            return 1.058 * Math.Exp(-0.00353 * B9_allowedTemperatureWorkClearedGas);
        }

        public static double D20_K5(double B23_DustConcentrationInWasteGas_Zo)
        {
            if (B23_DustConcentrationInWasteGas_Zo > 0.02)
            {
                return 1;
            }
            else
            {
                return 0.96;
            }
        }
        public static double D20_2(double B22_AverageDiameterDustParticles_dm)
        {
            return 1 - 79 * Math.Pow(B22_AverageDiameterDustParticles_dm / 1000000, 0.47);
        }

        public static double D21_MoreThenEnoghtDavlenie_A_K5(double B22_AverageDiameterDustParticles_dm, double h7, double stekl) // стеклоткань - 0,52
        {

            double d20_2 = D20_2(B22_AverageDiameterDustParticles_dm);

             return 670 * 0.000001* Math.Pow(1 - d20_2, 2)* Math.Pow(stekl, 3)* Math.Pow(h7 * 1000, 0.666666666666)/(Math.Pow(B22_AverageDiameterDustParticles_dm * 0.000001, 1.75)* Math.Pow(d20_2, 3)) ;

        }
        public static double D22_Δp1(double h7, double stekl, double B22_AverageDiameterDustParticles_dm, double B6_rashodDryClearedGasForNornalSituation, double B18_waterSteamWhichContainsInOutComingGas, double B19_waterSteamWhichContainsInAtmosphereAir, double B20_BarometricPressureGround_Pbar, double B7_temperatureComeOutGasForOchistka, double B16_gasQUnderHisAllowedTemperature, double B9_allowedTemperatureWorkClearedGas, double B15_gasQUnderHisTemperature, double B17_QAtmosphereAirUnderAllowedGasTemperature, double B8_airTemperature, double B21_ExcessGasPressure_Rg, double B12_gasViskosityNormalSituation, double B3_sterlingConstGas, double B13_airViskosityNormalSituation, double B4_sterlingConstAir, double B14_waterViskosityNormalSituation, double B5_sterlingConstWater, double B31_FiltrationRate_ωf)
        {
            double d13 = D13_u_smeshan_gas( B6_rashodDryClearedGasForNornalSituation,  B18_waterSteamWhichContainsInOutComingGas,  B19_waterSteamWhichContainsInAtmosphereAir,  B20_BarometricPressureGround_Pbar,  B7_temperatureComeOutGasForOchistka,  B16_gasQUnderHisAllowedTemperature,  B9_allowedTemperatureWorkClearedGas,  B15_gasQUnderHisTemperature,  B17_QAtmosphereAirUnderAllowedGasTemperature,  B8_airTemperature,  B21_ExcessGasPressure_Rg,  B12_gasViskosityNormalSituation,  B3_sterlingConstGas,  B13_airViskosityNormalSituation,  B4_sterlingConstAir,  B14_waterViskosityNormalSituation,  B5_sterlingConstWater);
            double d21 = D21_MoreThenEnoghtDavlenie_A_K5(B22_AverageDiameterDustParticles_dm,  h7,  stekl);
            return d13 * B31_FiltrationRate_ωf * d21 / 60000;
        }

        //  Допустимое гидравлическое сопротивление фильтра 
        public static double D23(double h7, double stekl, double B22_AverageDiameterDustParticles_dm, double B6_rashodDryClearedGasForNornalSituation, double B18_waterSteamWhichContainsInOutComingGas, double B19_waterSteamWhichContainsInAtmosphereAir, double B20_BarometricPressureGround_Pbar, double B7_temperatureComeOutGasForOchistka, double B16_gasQUnderHisAllowedTemperature, double B9_allowedTemperatureWorkClearedGas, double B15_gasQUnderHisTemperature, double B17_QAtmosphereAirUnderAllowedGasTemperature, double B8_airTemperature, double B21_ExcessGasPressure_Rg, double B12_gasViskosityNormalSituation, double B3_sterlingConstGas, double B13_airViskosityNormalSituation, double B4_sterlingConstAir, double B14_waterViskosityNormalSituation, double B5_sterlingConstWater, double B31_FiltrationRate_ωf)
        {
            double d20_2 = D20_2(B22_AverageDiameterDustParticles_dm);
            double d22 = D22_Δp1(h7, stekl, B22_AverageDiameterDustParticles_dm,  B6_rashodDryClearedGasForNornalSituation,  B18_waterSteamWhichContainsInOutComingGas,  B19_waterSteamWhichContainsInAtmosphereAir,  B20_BarometricPressureGround_Pbar,  B7_temperatureComeOutGasForOchistka,  B16_gasQUnderHisAllowedTemperature,  B9_allowedTemperatureWorkClearedGas,  B15_gasQUnderHisTemperature,  B17_QAtmosphereAirUnderAllowedGasTemperature,  B8_airTemperature,  B21_ExcessGasPressure_Rg,  B12_gasViskosityNormalSituation,  B3_sterlingConstGas,  B13_airViskosityNormalSituation,  B4_sterlingConstAir,  B14_waterViskosityNormalSituation,  B5_sterlingConstWater,  B31_FiltrationRate_ωf);
            return d22 + d20_2;
            ;
        }

        public static double D24_Δpк(double B29_ResistanceCoefficient_ξ, double B28_OptimalGasFilterSpeed_ωвх, double B20_BarometricPressureGround_Pbar, double B18_waterSteamWhichContainsInOutComingGas, double B9_allowedTemperatureWorkClearedGas, double B21_ExcessGasPressure_Rg, double B10_gasDensityNormalSituation, double B6_rashodDryClearedGasForNornalSituation, double B7_temperatureComeOutGasForOchistka, double B11_airDensityNormalSituation, double B19_waterSteamWhichContainsInAtmosphereAir, double B16_gasQUnderHisAllowedTemperatur, double B15_gasQUnderHisTemperature, double B17_QAtmosphereAirUnderAllowedGasTemperature, double B8_airTemperature, double B16_gasQUnderHisAllowedTemperature)
        {
            double d9 = D9_p_o_g_vl(B20_BarometricPressureGround_Pbar, B18_waterSteamWhichContainsInOutComingGas, B9_allowedTemperatureWorkClearedGas, B21_ExcessGasPressure_Rg, B10_gasDensityNormalSituation, B6_rashodDryClearedGasForNornalSituation, B7_temperatureComeOutGasForOchistka, B11_airDensityNormalSituation, B19_waterSteamWhichContainsInAtmosphereAir, B16_gasQUnderHisAllowedTemperatur, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature, B16_gasQUnderHisAllowedTemperature);
            return B29_ResistanceCoefficient_ξ * d9 * B28_OptimalGasFilterSpeed_ωвх * B28_OptimalGasFilterSpeed_ωвх / 2000;
        }


        public static double D25_B(double B22_AverageDiameterDustParticles_dm, double B24_DustParticleDensity_ρh)
        {
            double d20_2 = D20_2(B22_AverageDiameterDustParticles_dm);
            double raz = 817 * (1 - d20_2);
            double dva = (B22_AverageDiameterDustParticles_dm * Math.Pow(10, -6));
            double tri = Math.Pow(dva, 2);
            double chetyre = Math.Pow(d20_2, 3);
            double pyat = tri * chetyre * B24_DustParticleDensity_ρh;
            return Math.Round((raz / pyat), 0);
            
        }

        //Расчетное время процесса фильтрации
        public static double D26(double h7, double stekl, double B24_DustParticleDensity_ρh, double B23_DustConcentrationInWasteGas_Zo, double B22_AverageDiameterDustParticles_dm, double B6_rashodDryClearedGasForNornalSituation, double B18_waterSteamWhichContainsInOutComingGas, double B19_waterSteamWhichContainsInAtmosphereAir, double B20_BarometricPressureGround_Pbar, double B7_temperatureComeOutGasForOchistka, double B16_gasQUnderHisAllowedTemperature, double B9_allowedTemperatureWorkClearedGas, double B15_gasQUnderHisTemperature, double B17_QAtmosphereAirUnderAllowedGasTemperature, double B8_airTemperature, double B21_ExcessGasPressure_Rg, double B12_gasViskosityNormalSituation, double B3_sterlingConstGas, double B13_airViskosityNormalSituation, double B4_sterlingConstAir, double B14_waterViskosityNormalSituation, double B5_sterlingConstWater, double B31_FiltrationRate_ωf)
        {
            double d13 = D13_u_smeshan_gas(B6_rashodDryClearedGasForNornalSituation, B18_waterSteamWhichContainsInOutComingGas, B19_waterSteamWhichContainsInAtmosphereAir, B20_BarometricPressureGround_Pbar, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature, B21_ExcessGasPressure_Rg, B12_gasViskosityNormalSituation, B3_sterlingConstGas, B13_airViskosityNormalSituation, B4_sterlingConstAir, B14_waterViskosityNormalSituation, B5_sterlingConstWater);
            double d14 = D14_Z(B23_DustConcentrationInWasteGas_Zo, B6_rashodDryClearedGasForNornalSituation, B18_waterSteamWhichContainsInOutComingGas, B19_waterSteamWhichContainsInAtmosphereAir, B20_BarometricPressureGround_Pbar, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature, B21_ExcessGasPressure_Rg);
            double d21 = D21_MoreThenEnoghtDavlenie_A_K5(B22_AverageDiameterDustParticles_dm, h7, stekl);
            double d23 = D23(h7, stekl, B22_AverageDiameterDustParticles_dm, B6_rashodDryClearedGasForNornalSituation, B18_waterSteamWhichContainsInOutComingGas, B19_waterSteamWhichContainsInAtmosphereAir, B20_BarometricPressureGround_Pbar, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature, B21_ExcessGasPressure_Rg, B12_gasViskosityNormalSituation, B3_sterlingConstGas, B13_airViskosityNormalSituation, B4_sterlingConstAir, B14_waterViskosityNormalSituation, B5_sterlingConstWater, B31_FiltrationRate_ωf);
            double d25 = D25_B(B22_AverageDiameterDustParticles_dm, B24_DustParticleDensity_ρh);
            double raz = d13 * (B31_FiltrationRate_ωf / 60);
            double dva = 1000 * d23;
            double tri = Math.Round(dva / raz, 5);
            return (tri - d21) / ((d25 * (B31_FiltrationRate_ωf / 60) * (d14 / 1000)));
        }

        //Расчетное количество регенераций фильтра за 1 час работы, 1/с np
        public static double D27(double h7, double stekl, double B32_RecommendedFilterRegenerationTime, double B24_DustParticleDensity_ρh, double B23_DustConcentrationInWasteGas_Zo, double B22_AverageDiameterDustParticles_dm, double B6_rashodDryClearedGasForNornalSituation, double B18_waterSteamWhichContainsInOutComingGas, double B19_waterSteamWhichContainsInAtmosphereAir, double B20_BarometricPressureGround_Pbar, double B7_temperatureComeOutGasForOchistka, double B16_gasQUnderHisAllowedTemperature, double B9_allowedTemperatureWorkClearedGas, double B15_gasQUnderHisTemperature, double B17_QAtmosphereAirUnderAllowedGasTemperature, double B8_airTemperature, double B21_ExcessGasPressure_Rg, double B12_gasViskosityNormalSituation, double B3_sterlingConstGas, double B13_airViskosityNormalSituation, double B4_sterlingConstAir, double B14_waterViskosityNormalSituation, double B5_sterlingConstWater, double B31_FiltrationRate_ωf)
        {
            double d26 = D26(h7, stekl, B24_DustParticleDensity_ρh,  B23_DustConcentrationInWasteGas_Zo,  B22_AverageDiameterDustParticles_dm,  B6_rashodDryClearedGasForNornalSituation,  B18_waterSteamWhichContainsInOutComingGas,  B19_waterSteamWhichContainsInAtmosphereAir,  B20_BarometricPressureGround_Pbar,  B7_temperatureComeOutGasForOchistka,  B16_gasQUnderHisAllowedTemperature,  B9_allowedTemperatureWorkClearedGas,  B15_gasQUnderHisTemperature,  B17_QAtmosphereAirUnderAllowedGasTemperature,  B8_airTemperature,  B21_ExcessGasPressure_Rg,  B12_gasViskosityNormalSituation,  B3_sterlingConstGas,  B13_airViskosityNormalSituation,  B4_sterlingConstAir,  B14_waterViskosityNormalSituation,  B5_sterlingConstWater,  B31_FiltrationRate_ωf);
            return 3600 / (d26 + B32_RecommendedFilterRegenerationTime);
        }

        public static double D28_Vp(double h7, double stekl,  double B32_RecommendedFilterRegenerationTime, double B24_DustParticleDensity_ρh, double B23_DustConcentrationInWasteGas_Zo, double B22_AverageDiameterDustParticles_dm, double B6_rashodDryClearedGasForNornalSituation, double B18_waterSteamWhichContainsInOutComingGas, double B19_waterSteamWhichContainsInAtmosphereAir, double B20_BarometricPressureGround_Pbar, double B7_temperatureComeOutGasForOchistka, double B16_gasQUnderHisAllowedTemperature, double B9_allowedTemperatureWorkClearedGas, double B15_gasQUnderHisTemperature, double B17_QAtmosphereAirUnderAllowedGasTemperature, double B8_airTemperature, double B21_ExcessGasPressure_Rg, double B12_gasViskosityNormalSituation, double B3_sterlingConstGas, double B13_airViskosityNormalSituation, double B4_sterlingConstAir, double B14_waterViskosityNormalSituation, double B5_sterlingConstWater, double B31_FiltrationRate_ωf)
        {
            double d5 = D5_Vg(B6_rashodDryClearedGasForNornalSituation, B18_waterSteamWhichContainsInOutComingGas, B19_waterSteamWhichContainsInAtmosphereAir, B20_BarometricPressureGround_Pbar, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature, B21_ExcessGasPressure_Rg);
            double d27 = D27(h7, stekl, B32_RecommendedFilterRegenerationTime, B24_DustParticleDensity_ρh, B23_DustConcentrationInWasteGas_Zo, B22_AverageDiameterDustParticles_dm, B6_rashodDryClearedGasForNornalSituation, B18_waterSteamWhichContainsInOutComingGas, B19_waterSteamWhichContainsInAtmosphereAir, B20_BarometricPressureGround_Pbar, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature, B21_ExcessGasPressure_Rg, B12_gasViskosityNormalSituation, B3_sterlingConstGas, B13_airViskosityNormalSituation, B4_sterlingConstAir, B14_waterViskosityNormalSituation, B5_sterlingConstWater, B31_FiltrationRate_ωf);
            return d5 * d27 * B32_RecommendedFilterRegenerationTime / 3600;
        }
        public static double D29_Ff(double h7, double stekl, double D16_K1, double B33_standardGasFilterLoad, double B32_RecommendedFilterRegenerationTime, double B24_DustParticleDensity_ρh, double B23_DustConcentrationInWasteGas_Zo, double B22_AverageDiameterDustParticles_dm, double B6_rashodDryClearedGasForNornalSituation, double B18_waterSteamWhichContainsInOutComingGas, double B19_waterSteamWhichContainsInAtmosphereAir, double B20_BarometricPressureGround_Pbar, double B7_temperatureComeOutGasForOchistka, double B16_gasQUnderHisAllowedTemperature, double B9_allowedTemperatureWorkClearedGas, double B15_gasQUnderHisTemperature, double B17_QAtmosphereAirUnderAllowedGasTemperature, double B8_airTemperature, double B21_ExcessGasPressure_Rg, double B12_gasViskosityNormalSituation, double B3_sterlingConstGas, double B13_airViskosityNormalSituation, double B4_sterlingConstAir, double B14_waterViskosityNormalSituation, double B5_sterlingConstWater, double B31_FiltrationRate_ωf)
        {
            double d5 = D5_Vg(B6_rashodDryClearedGasForNornalSituation, B18_waterSteamWhichContainsInOutComingGas, B19_waterSteamWhichContainsInAtmosphereAir, B20_BarometricPressureGround_Pbar, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature, B21_ExcessGasPressure_Rg);
            double d15 = D15_qf(B22_AverageDiameterDustParticles_dm, B33_standardGasFilterLoad, D16_K1, B23_DustConcentrationInWasteGas_Zo, B6_rashodDryClearedGasForNornalSituation, B18_waterSteamWhichContainsInOutComingGas, B19_waterSteamWhichContainsInAtmosphereAir, B20_BarometricPressureGround_Pbar, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature, B21_ExcessGasPressure_Rg);
            double d28 = D28_Vp(h7, stekl, B32_RecommendedFilterRegenerationTime, B24_DustParticleDensity_ρh, B23_DustConcentrationInWasteGas_Zo, B22_AverageDiameterDustParticles_dm, B6_rashodDryClearedGasForNornalSituation, B18_waterSteamWhichContainsInOutComingGas, B19_waterSteamWhichContainsInAtmosphereAir, B20_BarometricPressureGround_Pbar, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature, B21_ExcessGasPressure_Rg, B12_gasViskosityNormalSituation, B3_sterlingConstGas, B13_airViskosityNormalSituation, B4_sterlingConstAir, B14_waterViskosityNormalSituation, B5_sterlingConstWater, B31_FiltrationRate_ωf);
            return (d5 + d28) / (60 * d15);
        }

        public static double D30(double G9, double G8)
        {
            return G9 / G8;
        }

        public static double D31(double h7, double stekl, double G9, double B32_RecommendedFilterRegenerationTime, double B24_DustParticleDensity_ρh, double B23_DustConcentrationInWasteGas_Zo, double B22_AverageDiameterDustParticles_dm, double B6_rashodDryClearedGasForNornalSituation, double B18_waterSteamWhichContainsInOutComingGas, double B19_waterSteamWhichContainsInAtmosphereAir, double B20_BarometricPressureGround_Pbar, double B7_temperatureComeOutGasForOchistka, double B16_gasQUnderHisAllowedTemperature, double B9_allowedTemperatureWorkClearedGas, double B15_gasQUnderHisTemperature, double B17_QAtmosphereAirUnderAllowedGasTemperature, double B8_airTemperature, double B21_ExcessGasPressure_Rg, double B12_gasViskosityNormalSituation, double B3_sterlingConstGas, double B13_airViskosityNormalSituation, double B4_sterlingConstAir, double B14_waterViskosityNormalSituation, double B5_sterlingConstWater, double B31_FiltrationRate_ωf)
        {
            double d27 = D27(h7, stekl, B32_RecommendedFilterRegenerationTime, B24_DustParticleDensity_ρh, B23_DustConcentrationInWasteGas_Zo, B22_AverageDiameterDustParticles_dm, B6_rashodDryClearedGasForNornalSituation, B18_waterSteamWhichContainsInOutComingGas, B19_waterSteamWhichContainsInAtmosphereAir, B20_BarometricPressureGround_Pbar, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature, B21_ExcessGasPressure_Rg, B12_gasViskosityNormalSituation, B3_sterlingConstGas, B13_airViskosityNormalSituation, B4_sterlingConstAir, B14_waterViskosityNormalSituation, B5_sterlingConstWater, B31_FiltrationRate_ωf);
            return G9 * d27 * B32_RecommendedFilterRegenerationTime / 3600;
        }

        public static double D32(double h7, double stekl, double G9, double B32_RecommendedFilterRegenerationTime, double B24_DustParticleDensity_ρh, double B23_DustConcentrationInWasteGas_Zo, double B22_AverageDiameterDustParticles_dm, double B6_rashodDryClearedGasForNornalSituation, double B18_waterSteamWhichContainsInOutComingGas, double B19_waterSteamWhichContainsInAtmosphereAir, double B20_BarometricPressureGround_Pbar, double B7_temperatureComeOutGasForOchistka, double B16_gasQUnderHisAllowedTemperature, double B9_allowedTemperatureWorkClearedGas, double B15_gasQUnderHisTemperature, double B17_QAtmosphereAirUnderAllowedGasTemperature, double B8_airTemperature, double B21_ExcessGasPressure_Rg, double B12_gasViskosityNormalSituation, double B3_sterlingConstGas, double B13_airViskosityNormalSituation, double B4_sterlingConstAir, double B14_waterViskosityNormalSituation, double B5_sterlingConstWater, double B31_FiltrationRate_ωf)
        {
            double d31 = D31(h7, stekl, G9, B32_RecommendedFilterRegenerationTime, B24_DustParticleDensity_ρh, B23_DustConcentrationInWasteGas_Zo, B22_AverageDiameterDustParticles_dm, B6_rashodDryClearedGasForNornalSituation, B18_waterSteamWhichContainsInOutComingGas, B19_waterSteamWhichContainsInAtmosphereAir, B20_BarometricPressureGround_Pbar, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature, B21_ExcessGasPressure_Rg, B12_gasViskosityNormalSituation, B3_sterlingConstGas, B13_airViskosityNormalSituation, B4_sterlingConstAir, B14_waterViskosityNormalSituation, B5_sterlingConstWater, B31_FiltrationRate_ωf);
            return 60 * d31 * B31_FiltrationRate_ωf;
        }

        public static double D33(double h7, double stekl, double D16_K1, double B33_standardGasFilterLoad, double G9, double B32_RecommendedFilterRegenerationTime, double B24_DustParticleDensity_ρh, double B23_DustConcentrationInWasteGas_Zo, double B22_AverageDiameterDustParticles_dm, double B6_rashodDryClearedGasForNornalSituation, double B18_waterSteamWhichContainsInOutComingGas, double B19_waterSteamWhichContainsInAtmosphereAir, double B20_BarometricPressureGround_Pbar, double B7_temperatureComeOutGasForOchistka, double B16_gasQUnderHisAllowedTemperature, double B9_allowedTemperatureWorkClearedGas, double B15_gasQUnderHisTemperature, double B17_QAtmosphereAirUnderAllowedGasTemperature, double B8_airTemperature, double B21_ExcessGasPressure_Rg, double B12_gasViskosityNormalSituation, double B3_sterlingConstGas, double B13_airViskosityNormalSituation, double B4_sterlingConstAir, double B14_waterViskosityNormalSituation, double B5_sterlingConstWater, double B31_FiltrationRate_ωf)
        {
            double d5 = D5_Vg(B6_rashodDryClearedGasForNornalSituation, B18_waterSteamWhichContainsInOutComingGas, B19_waterSteamWhichContainsInAtmosphereAir, B20_BarometricPressureGround_Pbar, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature, B21_ExcessGasPressure_Rg);
            double d15 = D15_qf(B22_AverageDiameterDustParticles_dm, B33_standardGasFilterLoad, D16_K1, B23_DustConcentrationInWasteGas_Zo, B6_rashodDryClearedGasForNornalSituation, B18_waterSteamWhichContainsInOutComingGas, B19_waterSteamWhichContainsInAtmosphereAir, B20_BarometricPressureGround_Pbar, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature, B21_ExcessGasPressure_Rg);
            double d31 = D31(h7, stekl, G9, B32_RecommendedFilterRegenerationTime, B24_DustParticleDensity_ρh, B23_DustConcentrationInWasteGas_Zo, B22_AverageDiameterDustParticles_dm, B6_rashodDryClearedGasForNornalSituation, B18_waterSteamWhichContainsInOutComingGas, B19_waterSteamWhichContainsInAtmosphereAir, B20_BarometricPressureGround_Pbar, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature, B21_ExcessGasPressure_Rg, B12_gasViskosityNormalSituation, B3_sterlingConstGas, B13_airViskosityNormalSituation, B4_sterlingConstAir, B14_waterViskosityNormalSituation, B5_sterlingConstWater, B31_FiltrationRate_ωf);
            double d32 = D32(h7, stekl, G9, B32_RecommendedFilterRegenerationTime, B24_DustParticleDensity_ρh,  B23_DustConcentrationInWasteGas_Zo,  B22_AverageDiameterDustParticles_dm,  B6_rashodDryClearedGasForNornalSituation,  B18_waterSteamWhichContainsInOutComingGas,  B19_waterSteamWhichContainsInAtmosphereAir,  B20_BarometricPressureGround_Pbar,  B7_temperatureComeOutGasForOchistka,  B16_gasQUnderHisAllowedTemperature,  B9_allowedTemperatureWorkClearedGas,  B15_gasQUnderHisTemperature,  B17_QAtmosphereAirUnderAllowedGasTemperature,  B8_airTemperature,  B21_ExcessGasPressure_Rg,  B12_gasViskosityNormalSituation,  B3_sterlingConstGas,  B13_airViskosityNormalSituation,  B4_sterlingConstAir,  B14_waterViskosityNormalSituation,  B5_sterlingConstWater,  B31_FiltrationRate_ωf);

            return (d5 + d32) / (60 * d15) + d31;
        }

        public static double D34(double h7, double stekl, double D16_K1, double B33_standardGasFilterLoad, double G9, double B32_RecommendedFilterRegenerationTime, double B24_DustParticleDensity_ρh, double B23_DustConcentrationInWasteGas_Zo, double B22_AverageDiameterDustParticles_dm, double B6_rashodDryClearedGasForNornalSituation, double B18_waterSteamWhichContainsInOutComingGas, double B19_waterSteamWhichContainsInAtmosphereAir, double B20_BarometricPressureGround_Pbar, double B7_temperatureComeOutGasForOchistka, double B16_gasQUnderHisAllowedTemperature, double B9_allowedTemperatureWorkClearedGas, double B15_gasQUnderHisTemperature, double B17_QAtmosphereAirUnderAllowedGasTemperature, double B8_airTemperature, double B21_ExcessGasPressure_Rg, double B12_gasViskosityNormalSituation, double B3_sterlingConstGas, double B13_airViskosityNormalSituation, double B4_sterlingConstAir, double B14_waterViskosityNormalSituation, double B5_sterlingConstWater, double B31_FiltrationRate_ωf)
        {
            double d33 = D33(h7, stekl, D16_K1, B33_standardGasFilterLoad, G9, B32_RecommendedFilterRegenerationTime, B24_DustParticleDensity_ρh, B23_DustConcentrationInWasteGas_Zo, B22_AverageDiameterDustParticles_dm, B6_rashodDryClearedGasForNornalSituation, B18_waterSteamWhichContainsInOutComingGas, B19_waterSteamWhichContainsInAtmosphereAir, B20_BarometricPressureGround_Pbar, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature, B21_ExcessGasPressure_Rg, B12_gasViskosityNormalSituation, B3_sterlingConstGas, B13_airViskosityNormalSituation, B4_sterlingConstAir, B14_waterViskosityNormalSituation, B5_sterlingConstWater, B31_FiltrationRate_ωf);
            return (G9 - d33) / G9 * 100;
        }


        public static double D35(double G8, double B32_RecommendedFilterRegenerationTime)
        {

            return (G8 - 1) * B32_RecommendedFilterRegenerationTime;
        }

        public static double D36(double G9, double f2, double B22_AverageDiameterDustParticles_dm, double B33_standardGasFilterLoad, double D16_K1, double B23_DustConcentrationInWasteGas_Zo, double B6_rashodDryClearedGasForNornalSituation, double B18_waterSteamWhichContainsInOutComingGas, double B19_waterSteamWhichContainsInAtmosphereAir, double B20_BarometricPressureGround_Pbar, double B7_temperatureComeOutGasForOchistka, double B16_gasQUnderHisAllowedTemperature, double B9_allowedTemperatureWorkClearedGas, double B15_gasQUnderHisTemperature, double B17_QAtmosphereAirUnderAllowedGasTemperature, double B8_airTemperature, double B21_ExcessGasPressure_Rg)
        {
            double d5 = D5_Vg(B6_rashodDryClearedGasForNornalSituation, B18_waterSteamWhichContainsInOutComingGas, B19_waterSteamWhichContainsInAtmosphereAir, B20_BarometricPressureGround_Pbar, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature, B21_ExcessGasPressure_Rg);
            double d15 = D15_qf(B22_AverageDiameterDustParticles_dm, B33_standardGasFilterLoad, D16_K1, B23_DustConcentrationInWasteGas_Zo, B6_rashodDryClearedGasForNornalSituation, B18_waterSteamWhichContainsInOutComingGas, B19_waterSteamWhichContainsInAtmosphereAir, B20_BarometricPressureGround_Pbar, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature, B21_ExcessGasPressure_Rg);
            double d30 = D30(G9, f2);
            return (d5 / 60 + d30 * d15) / (G9 - d30);
        }

        public static double D37(double G9, double f2, double B22_AverageDiameterDustParticles_dm, double B33_standardGasFilterLoad, double D16_K1, double B23_DustConcentrationInWasteGas_Zo, double B6_rashodDryClearedGasForNornalSituation, double B18_waterSteamWhichContainsInOutComingGas, double B19_waterSteamWhichContainsInAtmosphereAir, double B20_BarometricPressureGround_Pbar, double B7_temperatureComeOutGasForOchistka, double B16_gasQUnderHisAllowedTemperature, double B9_allowedTemperatureWorkClearedGas, double B15_gasQUnderHisTemperature, double B17_QAtmosphereAirUnderAllowedGasTemperature, double B8_airTemperature, double B21_ExcessGasPressure_Rg)
        {
            double d15 = D15_qf(B22_AverageDiameterDustParticles_dm, B33_standardGasFilterLoad, D16_K1, B23_DustConcentrationInWasteGas_Zo, B6_rashodDryClearedGasForNornalSituation, B18_waterSteamWhichContainsInOutComingGas, B19_waterSteamWhichContainsInAtmosphereAir, B20_BarometricPressureGround_Pbar, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature, B21_ExcessGasPressure_Rg);
            double d36 = D36(G9, f2, B22_AverageDiameterDustParticles_dm, B33_standardGasFilterLoad, D16_K1, B23_DustConcentrationInWasteGas_Zo, B6_rashodDryClearedGasForNornalSituation, B18_waterSteamWhichContainsInOutComingGas, B19_waterSteamWhichContainsInAtmosphereAir, B20_BarometricPressureGround_Pbar, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature, B21_ExcessGasPressure_Rg);
            return (d36 - d15) / d36 * 100;
        }

        public static double D38(double h7, double stekl, double B24_DustParticleDensity_ρh, double B23_DustConcentrationInWasteGas_Zo, double B22_AverageDiameterDustParticles_dm, double B6_rashodDryClearedGasForNornalSituation, double B18_waterSteamWhichContainsInOutComingGas, double B19_waterSteamWhichContainsInAtmosphereAir, double B20_BarometricPressureGround_Pbar, double B7_temperatureComeOutGasForOchistka, double B16_gasQUnderHisAllowedTemperature, double B9_allowedTemperatureWorkClearedGas, double B15_gasQUnderHisTemperature, double B17_QAtmosphereAirUnderAllowedGasTemperature, double B8_airTemperature, double B21_ExcessGasPressure_Rg, double B12_gasViskosityNormalSituation, double B3_sterlingConstGas, double B13_airViskosityNormalSituation, double B4_sterlingConstAir, double B14_waterViskosityNormalSituation, double B5_sterlingConstWater, double B31_FiltrationRate_ωf)
        {
            double d14 = D14_Z(B23_DustConcentrationInWasteGas_Zo, B6_rashodDryClearedGasForNornalSituation, B18_waterSteamWhichContainsInOutComingGas, B19_waterSteamWhichContainsInAtmosphereAir, B20_BarometricPressureGround_Pbar, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature, B21_ExcessGasPressure_Rg);
            double d26 = D26(h7, stekl, B24_DustParticleDensity_ρh, B23_DustConcentrationInWasteGas_Zo, B22_AverageDiameterDustParticles_dm, B6_rashodDryClearedGasForNornalSituation, B18_waterSteamWhichContainsInOutComingGas, B19_waterSteamWhichContainsInAtmosphereAir, B20_BarometricPressureGround_Pbar, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature, B21_ExcessGasPressure_Rg, B12_gasViskosityNormalSituation, B3_sterlingConstGas, B13_airViskosityNormalSituation, B4_sterlingConstAir, B14_waterViskosityNormalSituation, B5_sterlingConstWater, B31_FiltrationRate_ωf);

            return d14 * B31_FiltrationRate_ωf * d26 / 60;
        }


        public static double D39(double h7, double stekl, double f2,double G9, double B24_DustParticleDensity_ρh, double B23_DustConcentrationInWasteGas_Zo, double B22_AverageDiameterDustParticles_dm, double B6_rashodDryClearedGasForNornalSituation, double B18_waterSteamWhichContainsInOutComingGas, double B19_waterSteamWhichContainsInAtmosphereAir, double B20_BarometricPressureGround_Pbar, double B7_temperatureComeOutGasForOchistka, double B16_gasQUnderHisAllowedTemperature, double B9_allowedTemperatureWorkClearedGas, double B15_gasQUnderHisTemperature, double B17_QAtmosphereAirUnderAllowedGasTemperature, double B8_airTemperature, double B21_ExcessGasPressure_Rg, double B12_gasViskosityNormalSituation, double B3_sterlingConstGas, double B13_airViskosityNormalSituation, double B4_sterlingConstAir, double B14_waterViskosityNormalSituation, double B5_sterlingConstWater, double B31_FiltrationRate_ωf)
        {
            double d30 = D30(G9, f2);
            double d38 = D38(h7, stekl, B24_DustParticleDensity_ρh, B23_DustConcentrationInWasteGas_Zo, B22_AverageDiameterDustParticles_dm, B6_rashodDryClearedGasForNornalSituation, B18_waterSteamWhichContainsInOutComingGas, B19_waterSteamWhichContainsInAtmosphereAir, B20_BarometricPressureGround_Pbar, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature, B21_ExcessGasPressure_Rg, B12_gasViskosityNormalSituation, B3_sterlingConstGas, B13_airViskosityNormalSituation, B4_sterlingConstAir, B14_waterViskosityNormalSituation, B5_sterlingConstWater, B31_FiltrationRate_ωf);
            return d38 * d30 / 1000;
        }
        public static double D40_poristostSloiaDustEp(double B22_AverageDiameterDustParticles_dm)
        {
            return (1 - 79 * Math.Pow((B22_AverageDiameterDustParticles_dm / 1000000), 0.47));
        }



    }
}

