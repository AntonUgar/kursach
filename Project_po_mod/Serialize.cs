﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_po_mod
{
    [Serializable]
    public class Serialize
    {
        public double DustParticleDensityProps { get; set; }
        public double PlotnostComeingOutD { get; set; }
        public double AverageRazmerDustProps { get; set; }
        public double rasHodGasProps { get; set; }
        public double SteamGasWhichContainsInProps { get; set; }
        public double waterSteamWhichContainsInAtmosphereAirProps { get; set; }
        public double BarometricPressureGround_PbarProps { get; set; }
        public double temperatureComeOutGasForOchistkaProps { get; set; }
        public double gasQUnderHisAllowedTemperatureProps { get; set; }
        public double allowedTemperatureWorkClearedGasProps { get; set; }
        public double gasQUnderHisTemperatureProps { get; set; }
        public double QAtmosphereAirUnderAllowedGasTemperatureProps { get; set; }
        public double airTemperatureProps { get; set; }
        public double ExcessGasPressure_RgProps { get; set; }
        public double gasViskosityNormalSituationProps { get; set; }
        public double FiltrationRateProps { get; set; }
        public double waterViskosityNormalSituationProps { get; set; }
        public double airViskosityNormalSituationProps { get; set; }
        public double PlotnostiPriNormalnoBratishkaGasProps { get; set; }
        public double PlotnostiPriNormalnoBratishkaAirProps { get; set; }
        public double OptimalnyiVyhodGasProps { get; set; }
        public double OptimalnyiResistanceProps { get; set; }
        public double allowedResistanceProps { get; set; }
    }
}
