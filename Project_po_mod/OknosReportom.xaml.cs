﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Project_po_mod
{
    /// <summary>
    /// Логика взаимодействия для OknosReportom.xaml
    /// </summary>
    public partial class OknosReportom : Window
    {
        List<ReportClass> rListforReport;
        public OknosReportom(List<ReportClass> rc)
        {
            InitializeComponent();
            ReportView.RefreshReport();
            this.rListforReport = rc;
            ReportView.Reset();
            DataTable dt = new DataTable();
            DataColumn colDate1 = new DataColumn("Name", typeof(string));
            dt.Columns.Add(colDate1);
            DataColumn colDate2 = new DataColumn("Value", typeof(string));
            dt.Columns.Add(colDate2);
            foreach (var a in rListforReport)
            {
                dt.Rows.Add(a.Name, a.Value);
            }
            ReportDataSource ds = new ReportDataSource("DataSet1",dt);
            ReportView.LocalReport.DataSources.Add(ds);
            ReportView.LocalReport.ReportEmbeddedResource = "Project_po_mod.Reportkk.rdlc";
            ReportView.RefreshReport();
        }
    }
}
